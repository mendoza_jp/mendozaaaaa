import React from 'react';
import {StyleSheet, SafeAreaView, TextInput, Button } from 'react-native';

export default class App extends React.Component {
  state= {TextBox: ''
  }
  TextBox = (text) => this.setState({TextBox: text})
  render() {
return (
  <SafeAreaView style = {styles.container}>
    <TextInput style = {styles.in}
      placeholder = "Enter Your Text Here"
      placeholderTextColor = "#777B7E"
      textAlign = "center"
      onChangeText = {this.TextBox}
      />
    <Button title = "Enter" onPress = {() => alert("You just typed: "+this.state.TextBox)}
    color = "black"/>
  </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  in: {
    margin: 30,
    height: 40,
    borderWidth: 2.5,
    width:300
  },
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    alignItems: "center",
    justifyContent: "center"
      }
  
});
